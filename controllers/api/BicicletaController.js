var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = (req, res) => {
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    });
}

exports.bicicleta_create = (req, res) => {
    const { id, color, modelo, lat, lng } = req.body;
    let bici = new Bicicleta(id, color, modelo, [lat, lng]);

    Bicicleta.add(bici);

    res.status(200).json({
        bicicleta: bici
    })
}

exports.bicicleta_delete = (req, res) => {
    Bicicleta.removeById(req.body.id);
    res.status(200).send();
}

exports.bicicleta_update = (req, res) => {
    const { id, color, modelo, lat, lng } = req.body;
    Bicicleta.updateById(id, color, modelo, lat, lng);
    res.status(200).send();
}