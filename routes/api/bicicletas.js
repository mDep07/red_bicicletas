const express = require('express');
const router = express.Router();
const BicicletaController = require('../../controllers/api/BicicletaController');

router.get('/', BicicletaController.bicicleta_list);
router.post('/create', BicicletaController.bicicleta_create);
router.delete('/delete', BicicletaController.bicicleta_delete);
router.post('/update', BicicletaController.bicicleta_update);

module.exports = router;