var mymap = L.map('main_map').setView([-30.709780, -62.005345], 13);

// L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
//     attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
//     maxZoom: 18,
//     id: 'mapbox/streets-v11',
//     tileSize: 512,
//     zoomOffset: -1,
//     accessToken: 'your.mapbox.access.token'
// }).addTo(mymap);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
}).addTo(mymap);


$.ajax({
    dataType: 'Json',
    url: 'api/bicicletas',
    success: (list) => {
        list.bicicletas.forEach(bici => {
            L.marker(bici.ubicacion, {title: bici.id})
                .addTo(mymap)
                .bindPopup(`${bici.id} | ${bici.color}`)
                .openPopup();
        });
    },
    error: (err) => {
        console.log(err);
    }
})

