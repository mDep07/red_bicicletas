const Bicicleta = function(id, color, modelo, ubicacion) {
    this.id = id;
    this.color = color;
    this.modelo = modelo,
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function() {
    return `id: ${this.id} | color: ${this.color}`;
}

Bicicleta.allBicis = [];
Bicicleta.add = function(bicis) {
    Bicicleta.allBicis.push(bicis);
}

Bicicleta.findById = (biciID) => {
    let bici = Bicicleta.allBicis.find(b => b.id == biciID);
    if(bici)
        return bici
    else 
        throw new Error(`No existe una bicicleta con el id: ${biciID}`);
}

Bicicleta.removeById = (biciID) => {
    // let bici = Bicicleta.findById(biciID);
    for(let i = 0; i < Bicicleta.allBicis.length; i++)
        if(Bicicleta.allBicis[i].id == biciID) {
            Bicicleta.allBicis.splice(i, 1);
            break;
        }
}

Bicicleta.updateById = (id, color, modelo, lat, lng) => {
    const bici = Bicicleta.findById(id);
    bici.color = color;
    bici.modelo = modelo;
    bici.ubicacion = [lat, lng];

    for(let i = 0; i < Bicicleta.allBicis.length; i++)
        if(Bicicleta.allBicis[i].id == id) {
            Bicicleta.allBicis.splice(i, 1);
            break;
        }

    Bicicleta.add(bici);
}


let bicicleta1 = new Bicicleta(1, 'Rojo', 'Fire Bire', [-30.703567, -62.010228]);
let bicicleta2 = new Bicicleta(2, 'Azul', 'Venzo', [-30.711666666667, -61.998611111111]);
Bicicleta.add(bicicleta1);
Bicicleta.add(bicicleta2);

module.exports = Bicicleta;